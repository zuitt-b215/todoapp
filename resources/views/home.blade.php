@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white bg-dark">Task App</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

                    <form method="post" action="/tasks">
                        @csrf
                        <input type="text" name="task" placeholder="add task">
                        <input type="text" name="status" value="pending">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </form>
                </div>

                @if(count($tasks)>0)
                    <table class="table table-header custom-table m-2 p-2">
                        <tr class="table-dark">
                           <th>Task</th>
                           <th>Status</th>
                           <th colspan="2">Actions</th> 
                        </tr>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->name }}</td>
                            <td>{{ $task->status }}</td>
                            @if(Auth::user()->id === $task->user_id)
                            <td>
                                <a class="btn btn-primary" type="button" href="/tasks/{{$task->id}}/edit">Edit</a>
                            </td>
                            <td>
                                <form method="post" action="/tasks/{{$task->id}}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                            @endif
                        </tr>    
                    @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
